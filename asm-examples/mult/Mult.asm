// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// initialize R2 and counter
    @R2
    M=0

    @i
    M=1

// main logic
(loop)
    @R1
    D=M
    @i
    D-M; JLT    // R1 > i ?

    @R0
    D=M
    @R2
    M=M+D       // R2 += R0

    @i
    M=M+1       // i++

    @loop
    0; JMP

(end)
    @end
    0; JMP