# Computer Layers

## Hardware layer

### Combinatorial Gates
- Nand(a=, b=, out=) - Nand gate (built-in)
##### Elementary logic gates
- Not(in=, out=) - Not gate
- And(a=, b=, out=) - And gate
- Or(a=, b=, out=) - Or gate
- Xor(a=, b=, out=) - Xor gate
- Mux(a=, b=, sel=, out=) - Selects between two inputs
- DMux(in=, sel=, a=, b=) - Channels the input to one out of two outputs
###### 16-bit variants
- Not16(in=, out=) - 16-bit Not
- And16(a=, b=, out=) - 16-bit And
- Or16(a=, b=, out=) - 16-bit Or
- Mux16(a=, b=, sel=, out=) - Selects between two 16-bit inputs 
###### Multi-way variants
- Or8Way(in=, out=) - 8-way Or 
- Mux4Way16(a=, b=, c=, d=, sel=, out=) - Selects between four 16-bit inputs
- Mux8Way16(a=, b=, c=, d=, e=, f=, g=, h=, sel=, out=) - Selects between eight 16-bit inputs
- DMux4Way(in=, sel=, a=, b=, c=, d=) - Channels the input to one out of four outputs
- DMux8Way(in=, sel=, a=, b=, c=, d=, e=, f=, g=, h=) - Channels the input to one out of eight outputs
##### Arithmetic gates
- HalfAdder(a=, b=, sum=, carry=) - Adds up 2 bits
- FullAdder(a=, b=, c=, sum=, carry=) - Adds up 3 bits
- Add16(a=, b=, out=) - Adds up two 16-bit two's complement values
- Inc16(in=, out=) - Sets out to in + 1
- ALU(x=, y=, zx=, nx=, zy=, ny=, f=, no=, out=, zr=, ng=) - Hack ALU

### Sequential gates
- DFF(in=, out=) - Data flip-flop gate (built-in)
##### Memory
- Bit(in=, load=, out=) - 1-bit register
- Register(in=, load=, out=) - 16-bit register
- RAM8(in=, load=, address=, out=) - 8-word RAM
- RAM64(in=, load=, address=, out=) - 64-word RAM
- RAM512(in=, load=, address=, out=) - 512-word RAM
- RAM4K(in=, load=, address=, out=) - 4K-word RAM
- RAM16K(in=, load=, address=, out=) - 16K-word RAM
- PC(in=, load=, inc=, reset=, out=) - Program Counter

### Computer architecture
##### Memory
- Screen(in=, load=, address=, out=) - Screen memory map (built-in)
- Keyboard(out=) - Keyboard memory map (built-in)
- Memory(in=, load=, address=, out=) - Data memory of the Hack platform (RAM)
##### CPU
- ARegister(in=, load=, out=) - Address register (built-in)
- DRegister(in=, load=, out=) - Data register (built-in)
- CPU(inM=, instruction=, reset=, outM=, writeM=, addressM=, pc=) - Hack CPU
##### Computer
- ROM32K(address=, out=) - Instruction memory of the Hack platform (ROM, built-in)
- Computer(reset=) - Hack Computer

## Software layer

### Assembler
- Main.kt - The Hack Assembler

### Machine language programs
- mult.asm - Multiplies R0 and R1 and stores the result in R2
- Fill.asm - listens to the keyboard input and blackens the screen when a any key is pressed