// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

(start)
// check any button pressed
    @KBD
    D=M
    @white
    D; JEQ  // button not pressed?

// fill black
(black)
    @color
    M=-1    // set 1111 1111 1111 1111 (full register of 1)
    @draw
    0; JMP

// fill white
(white)
    @color
    M=0

(draw)
    @SCREEN
    D=A
    @i
    M=D     // counter will be the register's address to color

    @8192
    D=D+A
    @last
    M=D     // last value is the end of SCREEN memory

(loop)
    @last
    D=M
    @i
    D=D-M
    @end
    D; JEQ

    @color
    D=M
    @i
    A=M
    M=D

    @i
    M=M+1

    @loop
    0; JMP

(end)
    @start
    0; JMP