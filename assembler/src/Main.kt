import java.io.File
import java.lang.System.out

fun main() {
    val dirOfFile = "assembler/pong"
    val nameOfFile = "Pong"
    val instructionsList = asmFileToInstructionsList("$dirOfFile/$nameOfFile.asm")
    val instructions = saveJumpLabels(instructionsList)
    val typedInstructions = parseListOfInstructions(instructions)
    val binaryRepresentation = typedInstructions.map(Instruction::toBinary)
    binaryRepresentation.forEach(out::println)
    File("assembler/out/$nameOfFile.hack").writeText(binaryRepresentation.joinToString("\n"))
}

fun saveJumpLabels(instructionsList: List<String>): List<String> {
    var cmdCnt = 0
    var varCnt = 16
    val instructions = mutableListOf<String>()
    for (instr in instructionsList) {
        if (instr.startsWith('(')) {
            val label = instr.substring(1, instr.length - 1)
            SymbolTable.t[label] = cmdCnt.toString()
        } else {
            cmdCnt++
        }
    }
    instructionsList.filter { !it.startsWith('(') }.forEach { s ->
        if (s.startsWith('@')) {
            var addr = s.substring(1)
            if (!addr.matches("^[0-9]+$".toRegex())) {
                if (SymbolTable.t[addr] == null) {
                    SymbolTable.t[addr] = varCnt.toString()
                    varCnt++
                }
                addr = SymbolTable.t[addr].toString()
            }
            instructions.add("@$addr")
        } else {
            instructions.add(s)
        }
    }
    return instructions
}

fun asmFileToInstructionsList(filename: String): List<String> = File(filename).readLines().map { it.substringBefore("//").replace("\\s".toRegex(), "") }.filter { it.isNotEmpty() }

fun parseListOfInstructions(instructions: List<String>): List<Instruction> = instructions.map { Instruction.parseInstruction(it) }

sealed class Instruction() {
    abstract fun toBinary(): String

    class AddressInstruction(val addressNumber: Int) : Instruction() {
        override fun toBinary(): String {
            val b = Integer.toBinaryString(addressNumber)
            val zr = 16 - b.length
            return "0".repeat(zr) + b
        }
    }

    class ComputeInstruction(val destinations: String, val computation: String, val jump: String) : Instruction() {
        override fun toBinary(): String {
            return "111" + Codes.ComputationCommandsMap[computation] + Codes.DestinationCodes[destinations] + Codes.JumpCodes[jump]
        }
    }

    companion object {
        fun parseInstruction(s: String): Instruction =
                if (s.startsWith('@')) {
                    AddressInstruction(s.substring(1).toInt())
                } else {
                    val l = s.split('=', ';')
                    if (l.size == 3) {
                        ComputeInstruction(l[0], l[1], l[2])
                    } else {
                        if (s.contains('=')) {
                            ComputeInstruction(l[0], l[1], "")
                        } else {
                            ComputeInstruction("", l[0], l[1])
                        }
                    }
                }
    }
}

object Codes {
    val ComputationCommandsMap = mapOf(
            "0" to "0101010",
            "1" to "0111111",
            "-1" to "0111010",
            "D" to "0001100",
            "A" to "0110000",
            "M" to "1110000",
            "!D" to "0001111",
            "!A" to "0110001",
            "!M" to "1110001",
            "-D" to "0001111",
            "-A" to "0110011",
            "-M" to "1110011",
            "D+1" to "0011111",
            "A+1" to "0110111",
            "M+1" to "1110111",
            "D-1" to "0001110",
            "A-1" to "0110010",
            "M-1" to "1110010",
            "D+A" to "0000010",
            "D+M" to "1000010",
            "D-A" to "0010011",
            "D-M" to "1010011",
            "A-D" to "0000111",
            "M-D" to "1000111",
            "D&A" to "0000000",
            "D&M" to "1000000",
            "D|A" to "0010101",
            "D|M" to "1010101"
    )

    val DestinationCodes = mapOf(
            "" to "000",
            "M" to "001",
            "D" to "010",
            "MD" to "011",
            "A" to "100",
            "AM" to "101",
            "AD" to "110",
            "AMD" to "111"
    )

    val JumpCodes = mapOf(
            "" to "000",
            "JGT" to "001",
            "JEQ" to "010",
            "JGE" to "011",
            "JLT" to "100",
            "JNE" to "101",
            "JLE" to "110",
            "JMP" to "111"
    )
}

// symbol-table
object SymbolTable {
    val t = mutableMapOf(
            "R0" to "0",
            "R1" to "1",
            "R2" to "2",
            "R3" to "3",
            "R4" to "4",
            "R5" to "5",
            "R6" to "6",
            "R7" to "7",
            "R8" to "8",
            "R9" to "9",
            "R10" to "10",
            "R11" to "11",
            "R12" to "12",
            "R13" to "13",
            "R14" to "14",
            "R15" to "15",

            "SCREEN" to "16384",
            "KBD" to "24576",

            "SP" to "0",
            "LCL" to "1",
            "ARG" to "2",
            "THIS" to "3",
            "THAT" to "4"
    )
}
